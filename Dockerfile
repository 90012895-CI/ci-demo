FROM bitnami/nginx:1.16

WORKDIR /app

COPY /dist .

COPY config/nginx.conf /opt/bitnami/nginx/conf/server_blocks/nginx.conf